if (typeof jQuery === "undefined") {
    throw new Error("jQuery plugins need to be before this file");
}

//Extend morrisJS
(function() {
    var $, MyMorris;

    MyMorris = window.MyMorris = {};
    $ = jQuery;

    MyMorris = Object.create(Morris);

    MyMorris.Bar.prototype.defaults["labelTop"] = false;

    MyMorris.Bar.prototype.drawLabelTop = function(xPos, yPos, text) {
        var label;
        return label = this.raphael.text(xPos, yPos, text).attr('font-size', this.options.gridTextSize).attr('font-family', this.options.gridTextFamily).attr('font-weight', this.options.gridTextWeight).attr('fill', this.options.gridTextColor);
    };

    MyMorris.Bar.prototype.drawSeries = function() {
        var barWidth, bottom, groupWidth, idx, lastTop, left, leftPadding, numBars, row, sidx, size, spaceLeft, top, ypos, zeroPos;
        groupWidth = this.width / this.options.data.length;
        numBars = this.options.stacked ? 1 : this.options.ykeys.length;
        barWidth = (groupWidth * this.options.barSizeRatio - this.options.barGap * (numBars - 1)) / numBars;
        if (this.options.barSize) {
            barWidth = Math.min(barWidth, this.options.barSize);
        }
        spaceLeft = groupWidth - barWidth * numBars - this.options.barGap * (numBars - 1);
        leftPadding = spaceLeft / 2;
        zeroPos = this.ymin <= 0 && this.ymax >= 0 ? this.transY(0) : null;
        return this.bars = (function() {
            var _i, _len, _ref, _results;
            _ref = this.data;
            _results = [];
            for (idx = _i = 0, _len = _ref.length; _i < _len; idx = ++_i) {
                row = _ref[idx];
                lastTop = 0;
                _results.push((function() {
                    var _j, _len1, _ref1, _results1;
                    _ref1 = row._y;
                    _results1 = [];
                    for (sidx = _j = 0, _len1 = _ref1.length; _j < _len1; sidx = ++_j) {
                        ypos = _ref1[sidx];
                        if (ypos !== null) {
                            if (zeroPos) {
                                top = Math.min(ypos, zeroPos);
                                bottom = Math.max(ypos, zeroPos);
                            } else {
                                top = ypos;
                                bottom = this.bottom;
                            }
                            left = this.left + idx * groupWidth + leftPadding;
                            if (!this.options.stacked) {
                                left += sidx * (barWidth + this.options.barGap);
                            }
                            size = bottom - top;
                            if (this.options.verticalGridCondition && this.options.verticalGridCondition(row.x)) {
                                this.drawBar(this.left + idx * groupWidth, this.top, groupWidth, Math.abs(this.top - this.bottom), this.options.verticalGridColor, this.options.verticalGridOpacity, this.options.barRadius, row.y[sidx]);
                            }
                            if (this.options.stacked) {
                                top -= lastTop;
                            }
                            this.drawBar(left, top, barWidth, size, this.colorFor(row, sidx, 'bar'), this.options.barOpacity, this.options.barRadius);
                            _results1.push(lastTop += size);

                            if (this.options.labelTop && !this.options.stacked) {
                                label = this.drawLabelTop((left + (barWidth / 2)), top - 10, row.y[sidx]);
                                textBox = label.getBBox();
                                _results.push(textBox);
                            }
                        } else {
                            _results1.push(null);
                        }
                    }
                    return _results1;
                }).call(this));
            }
            return _results;
        }).call(this);
    };
}).call(this);


$.AdminBSB = {};
$.AdminBSB.options = {
    colors: {
        red: '#F44336',
        pink: '#E91E63',
        purple: '#9C27B0',
        deepPurple: '#673AB7',
        indigo: '#3F51B5',
        blue: '#2196F3',
        lightBlue: '#03A9F4',
        cyan: '#00BCD4',
        teal: '#009688',
        green: '#4CAF50',
        lightGreen: '#8BC34A',
        lime: '#CDDC39',
        yellow: '#ffe821',
        amber: '#FFC107',
        orange: '#FF9800',
        deepOrange: '#FF5722',
        brown: '#795548',
        grey: '#9E9E9E',
        blueGrey: '#607D8B',
        black: '#000000',
        white: '#ffffff'
    },
    leftSideBar: {
        scrollColor: 'rgba(0,0,0,0.5)',
        scrollWidth: '4px',
        scrollAlwaysVisible: false,
        scrollBorderRadius: '0',
        scrollRailBorderRadius: '0',
        scrollActiveItemWhenPageLoad: true,
        breakpointWidth: 1170
    },
    dropdownMenu: {
        effectIn: 'fadeIn',
        effectOut: 'fadeOut'
    }
}

/* Left Sidebar - Function =================================================================================================
 *  You can manage the left sidebar menu options
 *
 */
$.AdminBSB.leftSideBar = {
    activate: function () {
        var _this = this;
        var $body = $('body');
        var $overlay = $('.overlay');

        //Close sidebar
        $(window).click(function (e) {
            var $target = $(e.target);
            if (e.target.nodeName.toLowerCase() === 'i') {
                $target = $(e.target).parent();
            }

            if (!$target.hasClass('bars') && _this.isOpen() && $target.parents('#leftsidebar').length === 0) {
                if (!$target.hasClass('js-right-sidebar')) $overlay.fadeOut();
                $body.removeClass('overlay-open');
            }
        });

        $.each($('.menu-toggle.toggled'), function (i, val) {
            $(val).next().slideToggle(0);
        });

        //When page load
        $.each($('.menu .list li.active'), function (i, val) {
            var $activeAnchors = $(val).find('a:eq(0)');

            $activeAnchors.addClass('toggled');
            $activeAnchors.next().show();
        });

        //Collapse or Expand Menu
        $('.menu-toggle').on('click', function (e) {
            var $this = $(this);
            var $content = $this.next();

            if ($($this.parents('ul')[0]).hasClass('list')) {
                var $not = $(e.target).hasClass('menu-toggle') ? e.target : $(e.target).parents('.menu-toggle');

                $.each($('.menu-toggle.toggled').not($not).next(), function (i, val) {
                    if ($(val).is(':visible')) {
                        $(val).prev().toggleClass('toggled');
                        $(val).slideUp();
                    }
                });
            }

            $this.toggleClass('toggled');
            $content.slideToggle(320);
        });

        //Set menu height
        _this.setMenuHeight();
        _this.checkStatuForResize(true);
        $(window).resize(function () {
            _this.setMenuHeight();
            _this.checkStatuForResize(false);
        });

        //Set Waves
        Waves.attach('.menu .list a', ['waves-block']);
        Waves.init();
    },
    setMenuHeight: function (isFirstTime) {
        if (typeof $.fn.slimScroll != 'undefined') {
            var configs = $.AdminBSB.options.leftSideBar;
            var height = ($(window).height() - ($('.legal').outerHeight() + $('.user-info').outerHeight() + $('.navbar').innerHeight()));
            var $el = $('.list');

            $el.slimscroll({
                height: height + "px",
                color: configs.scrollColor,
                size: configs.scrollWidth,
                alwaysVisible: configs.scrollAlwaysVisible,
                borderRadius: configs.scrollBorderRadius,
                railBorderRadius: configs.scrollRailBorderRadius
            });

            //Scroll active menu item when page load, if option set = true
            if ($.AdminBSB.options.leftSideBar.scrollActiveItemWhenPageLoad) {
                var activeItemOffsetTop = $('.menu .list li.active')[0].offsetTop
                if (activeItemOffsetTop > 150) $el.slimscroll({
                    scrollTo: activeItemOffsetTop + 'px'
                });
            }
        }
    },
    checkStatuForResize: function (firstTime) {
        var $body = $('body');
        var $openCloseBar = $('.navbar .navbar-header .bars');
        var width = $body.width();

        if (firstTime) {
            $body.find('.content, .sidebar').addClass('no-animate').delay(1000).queue(function () {
                $(this).removeClass('no-animate').dequeue();
            });
        }

        if (width < $.AdminBSB.options.leftSideBar.breakpointWidth) {
            $body.addClass('ls-closed');
            $openCloseBar.fadeIn();
        } else {
            $body.removeClass('ls-closed');
            $openCloseBar.fadeOut();
        }
    },
    isOpen: function () {
        return $('body').hasClass('overlay-open');
    }
};
//==========================================================================================================================

/* Right Sidebar - Function ================================================================================================
 *  You can manage the right sidebar menu options
 *
 */
$.AdminBSB.rightSideBar = {
    activate: function () {
        var _this = this;
        var $sidebar = $('#rightsidebar');
        var $overlay = $('.overlay');

        //Close sidebar
        $(window).click(function (e) {
            var $target = $(e.target);
            if (e.target.nodeName.toLowerCase() === 'i') {
                $target = $(e.target).parent();
            }

            if (!$target.hasClass('js-right-sidebar') && _this.isOpen() && $target.parents('#rightsidebar').length === 0) {
                if (!$target.hasClass('bars')) $overlay.fadeOut();
                $sidebar.removeClass('open');
            }
        });

        $('.js-right-sidebar').on('click', function () {
            $sidebar.toggleClass('open');
            if (_this.isOpen()) {
                $overlay.fadeIn();
            } else {
                $overlay.fadeOut();
            }
        });
    },
    isOpen: function () {
        return $('.right-sidebar').hasClass('open');
    }
};
//==========================================================================================================================

/* Searchbar - Function ================================================================================================
 *  You can manage the search bar
 *
 */
var $searchBar = $('.search-bar');
$.AdminBSB.search = {
    activate: function () {
        var _this = this;

        //Search button click event
        $('.js-search').on('click', function () {
            _this.showSearchBar();
        });

        //Close search click event
        $searchBar.find('.close-search').on('click', function () {
            _this.hideSearchBar();
        });

        //ESC key on pressed
        $searchBar.find('input[type="text"]').on('keyup', function (e) {
            if (e.keyCode == 27) {
                _this.hideSearchBar();
            }
        });
    },
    showSearchBar: function () {
        $searchBar.addClass('open');
        $searchBar.find('input[type="text"]').focus();
    },
    hideSearchBar: function () {
        $searchBar.removeClass('open');
        $searchBar.find('input[type="text"]').val('');
    }
};
//==========================================================================================================================

/* Navbar - Function =======================================================================================================
 *  You can manage the navbar
 *
 */
$.AdminBSB.navbar = {
    activate: function () {
        var $body = $('body');
        var $overlay = $('.overlay');

        //Open left sidebar panel
        $('.bars').on('click', function () {
            $body.toggleClass('overlay-open');
            if ($body.hasClass('overlay-open')) {
                $overlay.fadeIn();
            } else {
                $overlay.fadeOut();
            }
        });

        //Close collapse bar on click event
        $('.nav [data-close="true"]').on('click', function () {
            var isVisible = $('.navbar-toggle').is(':visible');
            var $navbarCollapse = $('.navbar-collapse');

            if (isVisible) {
                $navbarCollapse.slideUp(function () {
                    $navbarCollapse.removeClass('in').removeAttr('style');
                });
            }
        });
    }
};
//==========================================================================================================================

/* Input - Function ========================================================================================================
 *  You can manage the inputs(also textareas) with name of class 'form-control'
 *
 */
$.AdminBSB.input = {
    activate: function () {
        //On focus event
        $('.form-control').focus(function () {
            $(this).parent().addClass('focused');
        });

        //On focusout event
        $('.form-control').focusout(function () {
            var $this = $(this);
            if ($this.parents('.form-group').hasClass('form-float')) {
                if ($this.val() == '') {
                    $this.parents('.form-line').removeClass('focused');
                }
            } else {
                $this.parents('.form-line').removeClass('focused');
            }
        });

        //On label click
        $('body').on('click', '.form-float .form-line .form-label', function () {
            $(this).parent().find('input').focus();
        });

        //Not blank form
        $('.form-control').each(function () {
            if ($(this).val() !== '') {
                $(this).parents('.form-line').addClass('focused');
            }
        });
    }
};
//==========================================================================================================================

/* Form - Select - Function ================================================================================================
 *  You can manage the 'select' of form elements
 *
 */
$.AdminBSB.select = {
    activate: function () {
        if ($.fn.selectpicker) {
            $('select:not(.ms)').selectpicker();
        }
    }
};
//==========================================================================================================================

/* DropdownMenu - Function =================================================================================================
 *  You can manage the dropdown menu
 *
 */

$.AdminBSB.dropdownMenu = {
    activate: function () {
        var _this = this;

        $('.dropdown, .dropup, .btn-group').on({
            "show.bs.dropdown": function () {
                var dropdown = _this.dropdownEffect(this);
                _this.dropdownEffectStart(dropdown, dropdown.effectIn);
            },
            "shown.bs.dropdown": function () {
                var dropdown = _this.dropdownEffect(this);
                if (dropdown.effectIn && dropdown.effectOut) {
                    _this.dropdownEffectEnd(dropdown, function () {});
                }
            },
            "hide.bs.dropdown": function (e) {
                var dropdown = _this.dropdownEffect(this);
                if (dropdown.effectOut) {
                    e.preventDefault();
                    _this.dropdownEffectStart(dropdown, dropdown.effectOut);
                    _this.dropdownEffectEnd(dropdown, function () {
                        dropdown.dropdown.removeClass('open');
                    });
                }
            }
        });

        //Set Waves
        Waves.attach('.dropdown-menu li a', ['waves-block']);
        Waves.init();
    },
    dropdownEffect: function (target) {
        var effectIn = $.AdminBSB.options.dropdownMenu.effectIn,
            effectOut = $.AdminBSB.options.dropdownMenu.effectOut;
        var dropdown = $(target),
            dropdownMenu = $('.dropdown-menu', target);

        if (dropdown.length > 0) {
            var udEffectIn = dropdown.data('effect-in');
            var udEffectOut = dropdown.data('effect-out');
            if (udEffectIn !== undefined) {
                effectIn = udEffectIn;
            }
            if (udEffectOut !== undefined) {
                effectOut = udEffectOut;
            }
        }

        return {
            target: target,
            dropdown: dropdown,
            dropdownMenu: dropdownMenu,
            effectIn: effectIn,
            effectOut: effectOut
        };
    },
    dropdownEffectStart: function (data, effectToStart) {
        if (effectToStart) {
            data.dropdown.addClass('dropdown-animating');
            data.dropdownMenu.addClass('animated dropdown-animated');
            data.dropdownMenu.addClass(effectToStart);
        }
    },
    dropdownEffectEnd: function (data, callback) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        data.dropdown.one(animationEnd, function () {
            data.dropdown.removeClass('dropdown-animating');
            data.dropdownMenu.removeClass('animated dropdown-animated');
            data.dropdownMenu.removeClass(data.effectIn);
            data.dropdownMenu.removeClass(data.effectOut);

            if (typeof callback == 'function') {
                callback();
            }
        });
    }
};

//==========================================================================================================================

/* Drag and Drop Files - Function ================================================================================================
 *  You can manage the Drag and Drop Files options
 *
 */

var pieChartColors = [
    'rgb(255, 99, 132)',
    'rgb(255, 159, 64)',
    'rgb(255, 205, 86)',
    'rgb(75, 192, 192)',
    'rgb(54, 162, 235)',
    'rgb(153, 102, 255)',
    'rgb(201, 203, 207)',
    'rgba(0, 188, 212, 0.5)',
    "rgb(139, 195, 74)"
];
var pieChartColorsIndex = 0;
var count = 0;

var pieChartLabelsColorMapping = {};

var html = $('html'),
    showDrag = false,
    timeout = -1;

html.on('dragstart dragenter', function () {
    $('.page-drag-and-drop-wrapper').fadeIn();
    showDrag = true;
});
html.on('drop dragover', function (event) {
    showDrag = true;
    // Needed to allow effectAllowed, dropEffect to take effect
    event.stopPropagation();
    // Needed to allow effectAllowed, dropEffect to take effect
    event.preventDefault();
});
html.on('drop', function (event) {
    if ($.inArray('Files', event.originalEvent.dataTransfer.types) > -1) {

        $('.page-drag-and-drop-wrapper').fadeIn();
        showDrag = true;

        var reader = new FileReader();
        reader.onloadend = function () {
            var data = parseData(this.result);
            _sortData(data.categories);
            _initializeAllReports(data.categories);
            showDrag = false;
            $('.page-drag-and-drop-wrapper').fadeOut();
        };

        reader.readAsText(event.originalEvent.dataTransfer.files[0]);

    }
});
html.on('dragleave', function (e) {
    showDrag = false;
    clearTimeout(timeout);
    timeout = setTimeout(function () {
        if (!showDrag) {
            $('.page-drag-and-drop-wrapper').fadeOut();
        }
    }, 70);
});

//==========================================================================================================================


//==========================================================================================================================

/* Chart Initialization - Functions ================================================================================================
 *  This is the code where all charts are constructed
 *
 */

_initializeAllReports = data => {
    // $('.dashboard-main-container').empty();

    $('.dashboard--no-file-message').fadeOut();
    $('.globalContainer').fadeIn();
    $('.dashboard-charts-container').fadeIn();



    //First do the global section
    // // We generate the template for it
    // $('.dashboard-main-container').append([
    //     {title: "GLOBAL CHARTS", containerClass: "globalContainer", pieId: "globalDonut", sparklineId: "sparkline", sparklineBodyId: "sparklineBody", tableId: "globalTable", tableBodyId: "globalTableBody" },
    // ].map(templateString).join(''));

    //Global data for donut
    // initSentencesPieChart(data.sentences, "sentence_donut_chart");
    // initSentencesPieChart(data.words, "word_donut_chart");
    initBarChart(data, "accuracy_bar_chart");

    // //Global data for spark
    // initSparkline(data.dates, "sparkline", "sparklineBody");
    //
    // //Global data for tables
    // initTable(data.categories, "globalTable", "globalTableBody");
    //
    // //Global data Stacked bar
    // initStackBars(data.annotators);

    //Now we take care of the annotators
    for(var i = 0; i < data.length; i ++) {
        //First, we create the section
        $('.dashboard-main-container').append([
            {title: data[i].name.toUpperCase(), containerClass: count + "-pieContainer", pieId: count + "-pieId" },
        ].map(templateString).join(''));
        $('.' + count+ "-pieContainer").fadeIn();

        initPieChart(data[i].errors, (100 - data[i].value), count + "-pieId");
        count += 1;
    }
};

function _sortData(data) {
    function compare(a,b) {
        if (a.value < b.value)
            return 1;
        if (a.value > b.value)
            return -1;
        return 0;
    }

    data.sort(compare);
}

function parseData(text) {
    var lines = text.split('\n');
    var data = {
        categories: []
    };
    var categoryKey = 0;
    for (var i = 0; i < lines.length; i++) {
        var tabs = lines[i].split('\t');
        //Is a Category
        if(tabs.length==1 && !!tabs[0]) {
            var items = tabs[0].split("-");
            if(items.length <= 1) {
                continue;
            }
            var categoryName = items[0].replace(/ /g,'');
            var percentage = items[1].match(
                " (.*)%"
            );
            var splitted = percentage[0].split(" ");
            var percentageFloat = parseFloat(splitted[splitted.length - 1].slice(0, -1));

            data.categories.push({
                name: categoryName,
                value:  percentageFloat
            });
            categoryKey++;
        }

        //Is an error inside the category
        if(tabs.length==2) {
            var items = tabs[1].split("-");
            var errorName = items[0].replace(/ /g,'');
            var percentage = items[1].match(
                " (.*)%"
            );
            var splitted = percentage[0].split(" ");
            var percentageFloat = parseFloat(splitted[splitted.length - 1].slice(0, -1));

            if(data.categories[categoryKey-1].errors) {
                data.categories[categoryKey-1].errors.push({
                    name: errorName,
                    value:  percentageFloat
                });
            } else {
                data.categories[categoryKey-1]["errors"] = [];
                data.categories[categoryKey-1]["errors"].push({
                    name: errorName,
                    value:  percentageFloat
                });
            }

            //TODO create the charts
            console.log(data.categories);
        }
    }
    return data;
}

function initReportDateSort(data) {
    var key;

    //Sort array by date
    var sortable = [];
    var annotatorSortable = [];
    for (key in data.dates) {
        if (data.dates.hasOwnProperty(key)) {
            sortable.push(key);
        }
    }

    sortable.sort(function (a, b) {
        a = a.split('-').join('');
        b = b.split('-').join('');
        return a.localeCompare(b); // <-- alternative
    });

    //Convert array back into object
    var sortedDates = {};
    var sortedAnnotatorDates = {};
    for (var i = 0; i < sortable.length; i++) {
        sortedDates[sortable[i]] = data.dates[sortable[i]];
    }
    data.dates = sortedDates;


    var annotators = data.annotators;
    for (var property in annotators) {
        if (annotators.hasOwnProperty(property)) {
            annotatorSortable = [];
            for (key in annotators[property].dates) {
                if (annotators[property].dates.hasOwnProperty(key)) {
                    annotatorSortable.push(key);
                }
            }
            annotatorSortable.sort(function (a, b) {
                a = a.split('-').join('');
                b = b.split('-').join('');
                return a.localeCompare(b); // <-- alternative
            });

            //Convert array back into object
            sortedAnnotatorDates = {};
            for (var j = 0; j < annotatorSortable.length; j++) {
                sortedAnnotatorDates[annotatorSortable[j]] = annotators[property].dates[annotatorSortable[j]];
            }
            annotators[property].dates = sortedAnnotatorDates;
        }
    }
}

function initSentencesPieChart(data, element) {
    var chartData = [];
    var tmpData = [];
    var labels = [];
    var categoriesBreakdown = data;
    for (var property in categoriesBreakdown) {
        if (categoriesBreakdown.hasOwnProperty(property)) {
            chartData.push({
                label: property.charAt(0).toUpperCase() + property.slice(1),
                value: (categoriesBreakdown[property].percentage).toFixed(2)
            })
        }
    }

    //We now order the object alphabetically based on the label
    function compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const labelA = a.label.toUpperCase();
        const labelB = b.label.toUpperCase();

        let comparison = 0;
        if (labelA > labelB) {
            comparison = 1;
        } else if (labelA < labelB) {
            comparison = -1;
        }
        return comparison;
    }

    chartData = chartData.sort(compare);


    for (var i = 0; i < chartData.length; i++) {
        tmpData.push(chartData[i].value);
        labels.push(chartData[i].label);
        //If it doesn't exist, we map a color
        if (!pieChartLabelsColorMapping[chartData[i].label]) {
            pieChartLabelsColorMapping[chartData[i].label] = pieChartColors[pieChartColorsIndex];
            pieChartColorsIndex += 1;
            pieChartColorsIndex = pieChartColorsIndex % pieChartColors.length;
        }
    }

    //We generate the background color array in the order of appereance of the categories to make sure they match across graphs
    var backgroundColor = [];
    for (i = 0; i < labels.length; i++) {
        backgroundColor.push(pieChartLabelsColorMapping[labels[i]]);
    }

    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: tmpData,
                backgroundColor: backgroundColor,
            }],
            labels: labels
        },
        options: {
            pieceLabel: {
                // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                render: 'percentage'
            },
            responsive: true,
            legend: {
                display: true
            }
        }
    }
    var ctx = document.getElementById(element).getContext("2d");
    new Chart(ctx, config);
}

function initPieChart(data, total, element) {
    var chartData = [];
    var tmpData = [];
    var labels = [];
    for(var i = 0; i < data.length; i++) {
        chartData.push({
            label: data[i].name,
            value: ((data[i].value / total) * 100).toFixed(2)
        })
    }
    // var categoriesBreakdown = data;
    // for (var property in categoriesBreakdown) {
    //     if (categoriesBreakdown.hasOwnProperty(property)) {
    //         chartData.push({
    //             label: property,
    //             value: ((categoriesBreakdown[property] / total) * 100).toFixed(2)
    //         })
    //     }
    // }

    //We now order the object alphabetically based on the label
    function compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const labelA = a.label.toUpperCase();
        const labelB = b.label.toUpperCase();

        let comparison = 0;
        if (labelA > labelB) {
            comparison = 1;
        } else if (labelA < labelB) {
            comparison = -1;
        }
        return comparison;
    }

    chartData = chartData.sort(compare);


    for (var i = 0; i < chartData.length; i++) {
        tmpData.push(chartData[i].value);
        labels.push(chartData[i].label);
        //If it doesn't exist, we map a color
        if (!pieChartLabelsColorMapping[chartData[i].label]) {
            pieChartLabelsColorMapping[chartData[i].label] = pieChartColors[pieChartColorsIndex];
            pieChartColorsIndex += 1;
            pieChartColorsIndex = pieChartColorsIndex % pieChartColors.length;
        }
    }

    //We generate the background color array in the order of appereance of the categories to make sure they match across graphs
    var backgroundColor = [];
    for (i = 0; i < labels.length; i++) {
        backgroundColor.push(pieChartLabelsColorMapping[labels[i]]);
    }

    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: tmpData,
                backgroundColor: backgroundColor,
            }],
            labels: labels
        },
        options: {
            responsive: true,
            legend: {
                display: true
            },
            pieceLabel: {
                // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                render: 'percentage'
            },
        }
    };
    var ctx = document.getElementById(element).getContext("2d");
    new Chart(ctx, config);
}

function initBarChart(data, element) {
    Morris.Bar({
        element: document.getElementById(element),
        data: data,
        xkey: 'name',
        ykeys: ['value'],
        labels: ['Value'],
        xLabelMargin: 0,
        gridTextSize: 9,
        resize: true,
        xLabelAngle: 40,
        labelTop: true,
        "padding-bottom": 200,
        barColors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(96, 125, 139)']
    });

    // setTimeout(function () {
    //     $('#' + element + ' svg').css('height', '495px');
    // }, 500)

}

function initDonutChart(data, total, element) {
    var chartData = [];
    var categoriesBreakdown = data;
    for (var property in categoriesBreakdown) {
        if (categoriesBreakdown.hasOwnProperty(property)) {
            chartData.push({
                label: property,
                value: ((categoriesBreakdown[property] / total) * 100).toFixed(2)
            })
        }
    }
    Morris.Donut({
        element: element,
        data: chartData,
        colors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(96, 125, 139)'],
        formatter: function (y) {
            return y + '%'
        }
    });

}

function initSparkline(data, element, sparklineBodyId) {
    // $('.dashboard-card-container').fadeIn();
    var dateBreakdown = data;
    var values = [];
    for (var property in dateBreakdown) {
        if (dateBreakdown.hasOwnProperty(property)) {
            values.push(dateBreakdown[property]);
            $('<li />', {
                html: property + "<span class=\"pull-right\"><b>" + dateBreakdown[property] + "</b> <small>RECORDS</small></span>"
            }).appendTo($("." + sparklineBodyId));
        }
    }

    $("#" + element).html(values.toString());
    // $("#" + element).sparkline();
    $("#" + element).each(function () {
        var $this = $(this);
        $this["sparkline"]('html', $this.data());
    });
}

function initTable(data, tableId, tableBodyId) {
    var categoriesBreakdown = data;
    for (var property in categoriesBreakdown) {
        if (categoriesBreakdown.hasOwnProperty(property)) {
            $('<tr />', {
                html: "<td>" + property + "</td><td>" + categoriesBreakdown[property] + "</td>"
            }).appendTo($("." + tableBodyId))
        }
    }

    //Exportable table
    $('.' + tableId).DataTable({
        dom: 'Bfrtip',
        responsive: true,
        "pageLength": 5,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
}

function initStackBars(data) {
    //We will iterate the annotators and build and object with all annotations per day
    var chartObject = {};
    var obj;
    var names = {};
    var chartColors = [
        'rgb(255, 99, 132)',
        'rgb(255, 159, 64)',
        'rgb(255, 205, 86)',
        'rgb(75, 192, 192)',
        'rgb(54, 162, 235)',
        'rgb(153, 102, 255)',
        'rgb(201, 203, 207)',
        'rgba(0, 188, 212, 0.5)',
        "rgb(139, 195, 74)"
    ];
    for (var property in data) {
        if (data.hasOwnProperty(property)) {
            for (var innerProperty in data[property]["dates"]) {
                if (data[property]["dates"].hasOwnProperty(innerProperty)) {
                    if (!chartObject[innerProperty]) {
                        chartObject[innerProperty] = [];
                    }
                    obj = {};
                    obj[data[property]["name"]] = data[property]["dates"][innerProperty];
                    chartObject[innerProperty].push(obj)
                    names[data[property]["name"]] = data[property]["name"];
                }
            }
        }
    }
    var labels = [];
    var datasets = [];
    var currentDateObj;
    var currentRecord,
        i,
        j,
        maxLength = 0,
        colorPickerIndex = 0;
    //we create the dataset for each user
    for (property in names) {
        if (names.hasOwnProperty(property)) {
            datasets.push({
                label: names[property],
                data: [],
                backgroundColor: chartColors[colorPickerIndex]
            });
            colorPickerIndex += 1;
            colorPickerIndex = colorPickerIndex % chartColors.length;
        }
    }

    for (property in chartObject) {
        if (chartObject.hasOwnProperty(property)) {
            labels.push(property);
            currentDateObj = chartObject[property];
            for (i = 0; i < currentDateObj.length; i++) {
                currentRecord = currentDateObj[i];
                //Retrieves the name of the user, which is the first property of the JS object
                for (j = 0; j < datasets.length; j++) {
                    if (datasets[j].label === Object.keys(currentRecord)[0]) {
                        datasets[j].data.push(currentRecord[Object.keys(currentRecord)[0]]);
                        maxLength < datasets[j].data.length ? maxLength = datasets[j].data.length : "";
                        break;
                    }
                }
            }

            //We use this to fill the rest of the users with 0s if there are not data for them in a specific date
            for (j = 0; j < datasets.length; j++) {
                if (datasets[j].data.length < maxLength) {
                    datasets[j].data.push(0);
                }
            }
        }
    }

    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    $('.dashboard-stackbar-chart').fadeIn();
    var ctx = document.getElementById('stackbar_chart').getContext('2d');
    new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: false,
            },
            tooltips: {
                mode: 'label',
                intersect: false
            },
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            }
        }
    });

}

const templateString = ({
                            title,
                            containerClass,
                            pieId
                        }) =>
    `<div class="dashboard-charts-container ${containerClass}">
                <div class="block-header">
                    <h2>${title}</h2>
                </div>
                    
                    <!-- Pie Chart -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="header">
                                <h2>ERROR BREAKDOWN</h2>
                            </div>
                            <div class="body">
                                <canvas id="${pieId}" height="173"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`;

// $('.dashboard-main-container').append([
//     { url: '/foo', img: 'foo.png', title: 'Foo item' },
// ].map(templateString).join(''));



//==========================================================================================================================

/* Browser - Function ======================================================================================================
 *  You can manage browser
 *
 */
var edge = 'Microsoft Edge';
var ie10 = 'Internet Explorer 10';
var ie11 = 'Internet Explorer 11';
var opera = 'Opera';
var firefox = 'Mozilla Firefox';
var chrome = 'Google Chrome';
var safari = 'Safari';

var extracted1 = function () {
    var browser = this.getBrowser();

    if (browser === edge) {
        return 'edge';
    } else if (browser === ie11) {
        return 'ie11';
    } else if (browser === ie10) {
        return 'ie10';
    } else if (browser === opera) {
        return 'opera';
    } else if (browser === chrome) {
        return 'chrome';
    } else if (browser === firefox) {
        return 'firefox';
    } else if (browser === safari) {
        return 'safari';
    } else {
        return '';
    }
};
$.AdminBSB.browser = {
    activate: function () {
        var _this = this;
        var className = _this.getClassName();

        if (className !== '') $('html').addClass(_this.getClassName());
    },
    getBrowser: function () {
        var userAgent = navigator.userAgent.toLowerCase();

        if (/edge/i.test(userAgent)) {
            return edge;
        } else if (/rv:11/i.test(userAgent)) {
            return ie11;
        } else if (/msie 10/i.test(userAgent)) {
            return ie10;
        } else if (/opr/i.test(userAgent)) {
            return opera;
        } else if (/chrome/i.test(userAgent)) {
            return chrome;
        } else if (/firefox/i.test(userAgent)) {
            return firefox;
        } else if (!!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/)) {
            return safari;
        }

        return undefined;
    },
    getClassName: extracted1
}
//==========================================================================================================================

function drawInlineSVG(svgElement, ctx, callback){

}

//usage :


$(function () {
    $.AdminBSB.browser.activate();
    // $.AdminBSB.leftSideBar.activate();
    $.AdminBSB.rightSideBar.activate();
    $.AdminBSB.navbar.activate();
    $.AdminBSB.dropdownMenu.activate();
    $.AdminBSB.input.activate();
    $.AdminBSB.select.activate();
    $.AdminBSB.search.activate();

    setTimeout(function () {
        $('.page-loader-wrapper').fadeOut();
    }, 50);
    document.getElementById('fileinput').addEventListener('change', function () {
        var file = this.files[0];

        var reader = new FileReader();
        reader.onloadend = function () {
            var data = parseData(this.result);
            _sortData(data.categories);
            _initializeAllReports(data.categories);
        };

        reader.readAsText(file);
    }, false);

    document.getElementById("printSSM").addEventListener("click", function () {
        var doc = new jsPDF();

        doc.text(35, 25, 'TOTAL ACCURACY');
        var mySVGelement = document.getElementById('accuracy_bar_chart').children[0];
        mySVGelement.toDataURL("image/png", {
            callback: function(data) {
                doc.addImage(data, 'PNG', 15, 35, 190, 80);
            }
        });

        for(var i = 0; i < count; i++) {
            doc.addPage();
            var sentenceChart = document.getElementById(i + "-pieId").toDataURL("image/png", 1.0);
            doc.text(35, 25, 'ERROR BREAKDOWN');
            doc.addImage(sentenceChart, 'JPEG', 15, 35, 190, 110);
        }

        // var sentenceChart = document.getElementById('sentence_donut_chart').toDataURL("image/png", 1.0);
        // var wordChart = document.getElementById('word_donut_chart').toDataURL("image/png", 1.0);
        //
        // doc.text(35, 25, 'SENTENCE ERROR BREAKDOWN');
        // doc.addImage(sentenceChart, 'JPEG', 15, 35);
        // doc.addPage();
        //
        // doc.text(35, 25, 'WORD ERROR BREAKDOWN');
        // doc.addImage(wordChart, 'JPEG', 15, 35);
        // doc.addPage();
        doc.save('asr-report.pdf');

    }, false);
});
